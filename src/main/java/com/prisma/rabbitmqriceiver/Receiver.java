package com.prisma.rabbitmqriceiver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
@Slf4j
public class Receiver {

    @RabbitListener(queues = "${conf-rabbitmq.queue-name}")
    public void receive(String in) {
        log.warn(" [x] Received by RabbitMQ'" + in + "'");

    }
}