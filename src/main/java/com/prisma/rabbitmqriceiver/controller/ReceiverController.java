package com.prisma.rabbitmqriceiver.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("${api.post-url}")
public class ReceiverController {

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void printMessage(@RequestBody String message){
        log.warn(" [x] Received by Post Request'" + message + "'");
    }
}
