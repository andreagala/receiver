package com.prisma.rabbitmqriceiver.component;

import com.prisma.rabbitmqriceiver.Receiver;
import lombok.Data;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("conf-rabbitmq")
@Data
public class RabbitComponent {

    private String queueName;

    @Bean
    Queue queue() {
        return new Queue(queueName, false, false, false);
    }

    @Bean
    Receiver receiver(){
        return new Receiver();
    }

}
